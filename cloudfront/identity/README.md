<!----------------------------------------------------------------------------->

# cloudfront/identity

#### Origin access identities for allowing [CloudFront] access to resources

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/cdn/aws//cloudfront/identity`**

--------------------------------------------------------------------------------

**NOTE**: This module requires that the **aws** provider is configured for
region `us-east-1`. For details on how to call individual modules with
specific providers, please [see here](https://www.terraform.io/upgrade-guides/0-11.html#interactions-between-providers-and-modules).

--------------------------------------------------------------------------------

### Example Usage

```
module "my_cloudfront_identity" {
  source = "gitlab.com/bitservices/cdn/aws//cloudfront/identity"

  providers = {
    "aws" = "aws.cloudfront"
  }
}
```

<!----------------------------------------------------------------------------->

[CloudFront]: https://aws.amazon.com/cloudfront/

<!----------------------------------------------------------------------------->
