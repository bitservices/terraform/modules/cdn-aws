################################################################################
# Optional Variables
################################################################################

variable "description" {
  type        = string
  default     = "Managed by Terraform"
  description = "An optional comment for the origin access identity."
}

################################################################################
# Resources
################################################################################

resource "aws_cloudfront_origin_access_identity" "scope" {
  comment = var.description
}

################################################################################
# Outputs
################################################################################

output "description" {
  value = var.description
}

################################################################################

output "id" {
  value = aws_cloudfront_origin_access_identity.scope.id
}

output "etag" {
  value = aws_cloudfront_origin_access_identity.scope.etag
}

output "iam_arn" {
  value = aws_cloudfront_origin_access_identity.scope.iam_arn
}

output "caller_reference" {
  value = aws_cloudfront_origin_access_identity.scope.caller_reference
}

output "access_identity_path" {
  value = aws_cloudfront_origin_access_identity.scope.cloudfront_access_identity_path
}

output "s3_canonical_user_id" {
  value = aws_cloudfront_origin_access_identity.scope.s3_canonical_user_id
}

################################################################################
