################################################################################
# Required Variables
################################################################################

variable "name" {
  type        = string
  description = "The full name of the CloudFront distribution."
}

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################

variable "domains" {
  type        = list(string)
  description = "A list of CNAME aliases for the CloudFront distribution."
}

################################################################################

variable "s3_identity" {
  type        = string
  description = "The CloudFront origin access identity to associate with the CloudFront distribution."
}

variable "s3_domain_name" {
  type        = string
  description = "The DNS domain name of the origin S3 bucket."
}


################################################################################
# Optional Variables
################################################################################

variable "ipv6" {
  type        = bool
  default     = true
  description = "Whether or not IPv6 is enabled for the distribution."
}

variable "enabled" {
  type        = bool
  default     = true
  description = "Whether the distribution is enabled to accept end user requests for content."
}

variable "protocol" {
  type        = string
  default     = "redirect-to-https"
  description = "Use this element to specify the protocol policy that applies to users accessing this CloudFront distribution."
}

variable "index_page" {
  type        = string
  default     = "index.html"
  description = "The object that you want CloudFront to return when an end user requests the root URL."
}

variable "description" {
  type        = string
  default     = "Managed by Terraform"
  description = "Any comments you want to include about the distribution."
}

variable "price_class" {
  type        = string
  default     = "PriceClass_100"
  description = "The price class for this distribution."
}

################################################################################

variable "compress_gzip" {
  type        = bool
  default     = true
  description = "Allow gzip compression."
}

variable "compress_brotli" {
  type        = bool
  default     = true
  description = "Allow brotli compression."
}

################################################################################

variable "methods_cached" {
  type        = list(string)
  default     = ["GET", "HEAD"]
  description = "Controls whether CloudFront caches the response to requests using the specified HTTP methods."
}

variable "methods_allowed" {
  type        = list(string)
  default     = ["GET", "HEAD"]
  description = "Controls which HTTP methods CloudFront processes and forwards to your Amazon S3 bucket."
}

################################################################################

variable "restriction_type" {
  type        = string
  default     = "none"
  description = "The method that you want to use to restrict distribution of your content by country."
}

variable "restriction_locations" {
  type        = list(string)
  default     = []
  description = "The ISO 3166-1-alpha-2 codes for which you want CloudFront either to distribute your content ('restriction_type' is 'whitelist') or not distribute your content ('restriction_type' is 'blacklist')."
}

################################################################################

variable "ssl_method" {
  type        = string
  default     = "sni-only"
  description = "Specifies how you want CloudFront to serve HTTPS requests."
}

variable "ssl_iam_arn" {
  type        = string
  default     = null
  description = "The ACM certificate arn of the custom viewer certificate for this distribution if you are using a custom domain. Certificate MUST be in 'us-east-1'. Takes precedence over 'ssl_iam_arn'. If this and 'ssl_iam_arn' are both left undefined, the default CloudFront certificate is used."
}

variable "ssl_acm_arn" {
  type        = string
  default     = null
  description = "The IAM certificate arn of the custom viewer certificate for this distribution if you are using a custom domain. If this and 'ssl_acm_arn' are both left undefined, the default CloudFront certificate is used."
}

variable "ssl_min_protocol" {
  type        = string
  default     = "TLSv1.2_2019"
  description = "The minimum version of the SSL protocol that you want CloudFront to use for HTTPS connections."
}

################################################################################

variable "error_page_404_ttl" {
  type        = number
  default     = null
  description = "The minimum amount of time you want HTTP 404 codes to stay in CloudFront caches before CloudFront queries your origin to see whether the object has been updated."
}

variable "error_page_404_code" {
  type        = number
  default     = 404
  description = "The HTTP status code that you want CloudFront to return with the custom error page to the viewer in case of a HTTP 404 error. Only has affect if 'error_page_404_path' is set."
}

variable "error_page_404_path" {
  type        = string
  default     = null
  description = "The path of the custom HTTP 404 error page."
}

################################################################################
# Locals
################################################################################

locals {
  compress            = var.compress_gzip || var.compress_brotli
  ssl_iam_arn         = var.ssl_acm_arn == null ? var.ssl_iam_arn : null
  ssl_use_default     = var.ssl_acm_arn == null && local.ssl_iam_arn == null ? true : false
  error_page_404_ttl  = coalesce(var.error_page_404_ttl, var.cache_policy_ttl_min)
  error_page_404_code = var.error_page_404_path != null ? var.error_page_404_code : null
}

################################################################################
# Resources
################################################################################

resource "aws_cloudfront_distribution" "scope" {
  aliases             = var.domains
  comment             = var.description
  enabled             = var.enabled
  price_class         = var.price_class
  is_ipv6_enabled     = var.ipv6
  default_root_object = var.index_page

  tags = {
    "Name"        = var.name
    "IPv6"        = var.ipv6
    "Owner"       = var.owner
    "Company"     = var.company
    "PriceClass"  = var.price_class
    "Compression" = local.compress ? "Enabled" : "Disabled"
  }

  origin {
    origin_id   = var.name
    domain_name = var.s3_domain_name

    s3_origin_config {
      origin_access_identity = var.s3_identity
    }
  }

  default_cache_behavior {
    compress               = local.compress
    cached_methods         = var.methods_cached
    allowed_methods        = var.methods_allowed
    cache_policy_id        = aws_cloudfront_cache_policy.scope.id
    target_origin_id       = var.name
    viewer_protocol_policy = var.protocol

    dynamic "lambda_function_association" {
      for_each = aws_lambda_function.scope

      content {
        event_type = lambda_function_association.value.tags.Event
        lambda_arn = lambda_function_association.value.qualified_arn
      }
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = var.restriction_type
      locations        = var.restriction_locations
    }
  }

  viewer_certificate {
    ssl_support_method             = var.ssl_method
    iam_certificate_id             = local.ssl_iam_arn
    acm_certificate_arn            = var.ssl_acm_arn
    minimum_protocol_version       = var.ssl_min_protocol
    cloudfront_default_certificate = local.ssl_use_default
  }

  custom_error_response {
    error_code            = 404
    response_code         = local.error_page_404_code
    response_page_path    = var.error_page_404_path
    error_caching_min_ttl = local.error_page_404_ttl
  }
}

################################################################################
# Outputs
################################################################################

output "name" {
  value = var.name

  depends_on = [
    aws_cloudfront_distribution.scope
  ]
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "domains" {
  value = var.domains
}

################################################################################

output "s3_identity" {
  value = var.s3_identity
}

output "s3_domain_name" {
  value = var.s3_domain_name
}

################################################################################

output "ipv6" {
  value = var.ipv6
}

output "enabled" {
  value = var.enabled
}

output "protocol" {
  value = var.protocol
}

output "index_page" {
  value = var.index_page
}

output "description" {
  value = var.description
}

output "price_class" {
  value = var.price_class
}

################################################################################

output "compress" {
  value = local.compress
}

output "compress_gzip" {
  value = var.compress_gzip
}

output "compress_brotli" {
  value = var.compress_brotli
}

################################################################################

output "methods_cached" {
  value = var.methods_cached
}

output "methods_allowed" {
  value = var.methods_allowed
}

################################################################################

output "restriction_type" {
  value = var.restriction_type
}

output "restriction_locations" {
  value = var.restriction_locations
}

################################################################################

output "ssl_method" {
  value = var.ssl_method
}

output "ssl_iam_arn" {
  value = local.ssl_iam_arn
}

output "ssl_acm_arn" {
  value = var.ssl_acm_arn
}

output "ssl_use_default" {
  value = local.ssl_use_default
}

output "ssl_min_protocol" {
  value = var.ssl_min_protocol
}

################################################################################

output "error_page_404_ttl" {
  value = local.error_page_404_ttl
}

output "error_page_404_code" {
  value = local.error_page_404_code
}

output "error_page_404_path" {
  value = var.error_page_404_path
}

################################################################################

output "id" {
  value = aws_cloudfront_distribution.scope.id
}

output "arn" {
  value = aws_cloudfront_distribution.scope.arn
}

output "etag" {
  value = aws_cloudfront_distribution.scope.etag
}

output "status" {
  value = aws_cloudfront_distribution.scope.status
}

output "domain_name" {
  value = aws_cloudfront_distribution.scope.domain_name
}

output "hosted_zone_id" {
  value = aws_cloudfront_distribution.scope.hosted_zone_id
}

output "trusted_signers" {
  value = aws_cloudfront_distribution.scope.trusted_signers
}

output "caller_reference" {
  value = aws_cloudfront_distribution.scope.caller_reference
}

output "last_modified_time" {
  value = aws_cloudfront_distribution.scope.last_modified_time
}

output "in_progress_validation_batches" {
  value = aws_cloudfront_distribution.scope.in_progress_validation_batches
}

################################################################################
