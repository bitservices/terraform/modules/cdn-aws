################################################################################
# Optional Variables
################################################################################

variable "route53_zone_name" {
  type        = string
  default     = "bitservices.io"
  description = "The DNS zone where Route53 records will be created for this CloudFront distribution."
}

variable "route53_zone_private" {
  type        = bool
  default     = false
  description = "Used with 'route53_zone_name' to find a private hosted zone."
}

################################################################################
# Data Sources
################################################################################

data "aws_route53_zone" "scope" {
  count        = var.route53_records_create ? 1 : 0
  name         = var.route53_zone_name
  private_zone = var.route53_zone_private
}

################################################################################
# Outputs
################################################################################

output "route53_zone_id" {
  value = length(data.aws_route53_zone.scope) == 1 ? data.aws_route53_zone.scope[0].id : null
}

output "route53_zone_name" {
  value = length(data.aws_route53_zone.scope) == 1 ? data.aws_route53_zone.scope[0].name : null
}

output "route53_zone_private" {
  value = length(data.aws_route53_zone.scope) == 1 ? data.aws_route53_zone.scope[0].private_zone : null
}

################################################################################
