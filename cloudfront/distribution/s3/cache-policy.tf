################################################################################
# Optional Variables
################################################################################

variable "cache_policy_class" {
  type        = string
  default     = "default"
  description = "Identifier to use for the cache policy within the CloudFront distribution."
}

variable "cache_policy_description" {
  type        = string
  default     = null
  description = "Any comments you want to include about the cache policy."
}

################################################################################

variable "cache_policy_cookies_list" {
  type        = list(string)
  default     = []
  description = "If you have specified 'cache_policy_cookies_policy' to 'whitelist' or 'allExcept', this is a list of cookies that the policy will apply to when CloudFront forwards to your origin S3 bucket."
}

variable "cache_policy_cookies_policy" {
  type        = string
  default     = "none"
  description = "Specifies whether you want CloudFront to forward cookies to the origin S3 bucket. Must be: 'none', 'whitelist', 'allExcept' or 'all'."

  validation {
    condition     = contains(["none", "whitelist", "allExcept", "all"], var.cache_policy_cookies_policy)
    error_message = "The CloudFront cache policy cookies policy must be: 'none', 'whitelist', 'allExcept' or 'all'."
  }
}

################################################################################

variable "cache_policy_headers_list" {
  type        = list(string)
  default     = []
  description = "If you have specified 'cache_policy_headers_policy' to 'whitelist', this is a list of headers that the policy will apply to when CloudFront forwards to your origin S3 bucket."
}

variable "cache_policy_headers_policy" {
  type        = string
  default     = "none"
  description = "Specifies whether you want CloudFront to forward headers to the origin S3 bucket. Must be: 'none' or 'whitelist'."

  validation {
    condition     = contains(["none", "whitelist"], var.cache_policy_headers_policy)
    error_message = "The CloudFront cache policy headers policy must be: 'none' or 'whitelist'."
  }
}

################################################################################

variable "cache_policy_query_string_list" {
  type        = list(string)
  default     = []
  description = "If you have specified 'cache_policy_query_string_policy' to 'whitelist' or 'allExcept', this is a list of query strings that the policy will apply to when CloudFront forwards to your origin S3 bucket."
}

variable "cache_policy_query_string_policy" {
  type        = string
  default     = "none"
  description = "Specifies whether you want CloudFront to forward query strings to the origin S3 bucket."

  validation {
    condition     = contains(["none", "whitelist", "allExcept", "all"], var.cache_policy_query_string_policy)
    error_message = "The CloudFront cache policy query string policy must be: 'none', 'whitelist', 'allExcept' or 'all'."
  }
}

################################################################################

variable "cache_policy_ttl_max" {
  type        = number
  default     = 86400
  description = "The maximum amount of time (in seconds) that an object is in a CloudFront cache before CloudFront forwards another request to your origin to determine whether the object has been updated. Only effective in the presence of 'Cache-Control max-age', 'Cache-Control s-maxage', and 'Expires' headers."
}

variable "cache_policy_ttl_min" {
  type        = number
  default     = 60
  description = "The minimum amount of time that you want objects to stay in CloudFront caches before CloudFront queries your origin to see whether the object has been updated."
}

variable "cache_policy_ttl_default" {
  type        = number
  default     = 3600
  description = "The default amount of time (in seconds) that an object is in a CloudFront cache before CloudFront forwards another request in the absence of a 'Cache-Control max-age' or 'Expires' header."
}

################################################################################
# Locals
################################################################################

locals {
  cache_policy_name              = format("%s-%s", var.name, var.cache_policy_class)
  cache_policy_description       = coalesce(var.cache_policy_description, var.description)
  cache_policy_cookies_list      = contains(["whitelist", "allExcept"], var.cache_policy_cookies_list) ? var.cache_policy_cookies_list : null
  cache_policy_headers_list      = contains(["whitelist"], var.cache_policy_headers_list) ? var.cache_policy_headers_list : null
  cache_policy_query_string_list = contains(["whitelist", "allExcept"], var.cache_policy_query_string_policy) ? var.cache_policy_query_string_list : null
}

################################################################################
# Resources
################################################################################

resource "aws_cloudfront_cache_policy" "scope" {
  name        = local.cache_policy_name
  comment     = local.cache_policy_description
  max_ttl     = var.cache_policy_ttl_max
  min_ttl     = var.cache_policy_ttl_min
  default_ttl = var.cache_policy_ttl_default

  parameters_in_cache_key_and_forwarded_to_origin {
    enable_accept_encoding_gzip   = var.compress_gzip
    enable_accept_encoding_brotli = var.compress_brotli

    cookies_config {
      cookie_behavior = var.cache_policy_cookies_policy

      dynamic "cookies" {
        for_each = local.cache_policy_cookies_list == null ? [] : [null]

        content {
          items = local.cache_policy_cookies_list
        }
      }
    }

    headers_config {
      header_behavior = var.cache_policy_headers_policy

      dynamic "headers" {
        for_each = local.cache_policy_headers_list == null ? [] : [null]

        content {
          items = local.cache_policy_headers_list
        }
      }
    }

    query_strings_config {
      query_string_behavior = var.cache_policy_query_string_policy

      dynamic "query_strings" {
        for_each = local.cache_policy_query_string_list == null ? [] : [null]

        content {
          items = local.cache_policy_query_string_list
        }
      }
    }
  }
}

################################################################################
# Outputs
################################################################################

output "cache_policy_class" {
  value = var.cache_policy_class
}

output "cache_policy_description" {
  value = local.cache_policy_description
}

################################################################################

output "cache_policy_cookies_list" {
  value = local.cache_policy_cookies_list
}

output "cache_policy_cookies_policy" {
  value = var.cache_policy_cookies_policy
}

################################################################################

output "cache_policy_headers_list" {
  value = local.cache_policy_headers_list
}

output "cache_policy_headers_policy" {
  value = var.cache_policy_headers_policy
}

################################################################################

output "cache_policy_query_string_list" {
  value = local.cache_policy_query_string_list
}

output "cache_policy_query_string_policy" {
  value = var.cache_policy_query_string_policy
}

################################################################################

output "cache_policy_ttl_max" {
  value = var.cache_policy_ttl_max
}

output "cache_policy_ttl_min" {
  value = var.cache_policy_ttl_min
}

output "cache_policy_ttl_default" {
  value = var.cache_policy_ttl_default
}

################################################################################

output "cache_policy_id" {
  value = aws_cloudfront_cache_policy.scope.id
}

output "cache_policy_etag" {
  value = aws_cloudfront_cache_policy.scope.etag
}

output "cache_policy_name" {
  value = aws_cloudfront_cache_policy.scope.name
}

################################################################################
