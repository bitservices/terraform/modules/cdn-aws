################################################################################
# Optional Variables
################################################################################

variable "route53_records_create" {
  type        = bool
  default     = false
  description = "If or not the DNS records in 'domains' should be created in Route53 under 'route53_zone_name' for this CloudFront distribution."
}

variable "route53_records_ipv4_type" {
  type        = string
  default     = "A"
  description = "The IPv4 record type. This should always be 'A'."
}

variable "route53_records_ipv6_type" {
  type        = string
  default     = "AAAA"
  description = "The IPv6 record type. This should always be 'AAAA'."
}

variable "route53_records_ipv4_target_health" {
  type        = bool
  default     = false
  description = "Set to 'true' if you want Route53 to determine whether to respond to IPv4 DNS queries using this resource record set by checking the health of the resource record set."
}

variable "route53_records_ipv6_target_health" {
  type        = bool
  default     = false
  description = "Set to 'true' if you want Route53 to determine whether to respond to IPv6 DNS queries using this resource record set by checking the health of the resource record set."
}

################################################################################
# Resources
################################################################################

resource "aws_route53_record" "ipv4" {
  count   = var.route53_records_create ? length(var.domains) : 0
  name    = var.domains[count.index]
  type    = var.route53_records_ipv4_type
  zone_id = data.aws_route53_zone.scope[0].id

  alias {
    name                   = aws_cloudfront_distribution.scope.domain_name
    zone_id                = aws_cloudfront_distribution.scope.hosted_zone_id
    evaluate_target_health = var.route53_records_ipv4_target_health
  }
}

resource "aws_route53_record" "ipv6" {
  count   = var.route53_records_create && var.ipv6 ? length(var.domains) : 0
  name    = var.domains[count.index]
  type    = var.route53_records_ipv6_type
  zone_id = data.aws_route53_zone.scope[0].id

  alias {
    name                   = aws_cloudfront_distribution.scope.domain_name
    zone_id                = aws_cloudfront_distribution.scope.hosted_zone_id
    evaluate_target_health = var.route53_records_ipv6_target_health
  }
}

################################################################################
# Outputs
################################################################################

output "route53_records_create" {
  value = var.route53_records_create
}

################################################################################

output "route53_records_ipv4_target_health" {
  value = var.route53_records_ipv4_target_health
}

################################################################################

output "route53_records_ipv6_target_health" {
  value = var.route53_records_ipv6_target_health
}

################################################################################

output "route53_records_ipv4_count" {
  value = length(aws_route53_record.ipv4)
}

output "route53_records_ipv4_names" {
  value = aws_route53_record.ipv4.*.name
}

output "route53_records_ipv4_types" {
  value = aws_route53_record.ipv4.*.type
}

################################################################################

output "route53_records_ipv6_count" {
  value = length(aws_route53_record.ipv6)
}

output "route53_records_ipv6_names" {
  value = aws_route53_record.ipv6.*.name
}

output "route53_records_ipv6_types" {
  value = aws_route53_record.ipv6.*.type
}

################################################################################
