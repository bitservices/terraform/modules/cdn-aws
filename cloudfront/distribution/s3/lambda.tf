################################################################################
# Optional Variables
################################################################################

variable "lambda_functions" {
  type = list(object({
    bucket       = string
    event        = string
    handler      = string
    publish      = bool
    runtime      = string
    version      = string
    function     = string
    memory_mb    = number
    description  = string
    timeout_secs = number
  }))
  default     = []
  description = "List of objects containing details of what [Lambda@Edge] functions to use with this [CloudFront] distribution."
}

################################################################################

variable "lambda_iam_path" {
  type        = string
  default     = "/"
  description = "The path to the IAM roles to be created for each Lambda@Edge function."
}

variable "lambda_iam_force_detach" {
  type        = bool
  default     = false
  description = "Specifies to force detach any other policies from the IAM roles created for each Lambda@Edge function when they are destroyed."
}

################################################################################

variable "lambda_iam_policy" {
  type        = string
  description = "The policy document that grants Lambda@Edge permission to assume the created IAM roles - in JSON format."

  default = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": [
          "edgelambda.amazonaws.com",
          "lambda.amazonaws.com"
        ]
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

################################################################################
# Resources
################################################################################

resource "aws_iam_role" "scope" {
  for_each              = zipmap(var.lambda_functions.*.function, var.lambda_functions.*)
  name                  = format("%s-%s", var.name, each.value.function)
  path                  = var.lambda_iam_path
  description           = each.value.description
  assume_role_policy    = var.lambda_iam_policy
  force_detach_policies = var.lambda_iam_force_detach
}

################################################################################

resource "aws_lambda_function" "scope" {
  for_each      = zipmap(var.lambda_functions.*.function, var.lambda_functions.*)
  role          = aws_iam_role.scope[each.key].arn
  s3_key        = format("%s/%s.zip", each.value.function, each.value.version)
  handler       = each.value.handler
  publish       = each.value.publish
  runtime       = each.value.runtime
  timeout       = each.value.timeout_secs
  s3_bucket     = each.value.bucket
  description   = each.value.description
  memory_size   = each.value.memory_mb
  function_name = format("%s-%s", var.name, each.value.function)

  tags = {
    "Name"         = format("%s-%s", var.name, each.value.function)
    "Event"        = each.value.event
    "Owner"        = var.owner
    "Memory"       = format("%s MB", each.value.memory_mb)
    "Company"      = var.company
    "Runtime"      = each.value.runtime
    "Timeout"      = format("%ss", each.value.timeout_secs)
    "Version"      = each.value.version
    "Function"     = each.value.function
    "Distribution" = var.name
  }
}

################################################################################
# Outputs
################################################################################

output "lambda_functions" {
  value = var.lambda_functions
}

################################################################################

output "lambda_iam_path" {
  value = var.lambda_iam_path
}

output "lambda_iam_force_detach" {
  value = var.lambda_iam_force_detach
}

################################################################################

output "lambda_iam_policy" {
  value = var.lambda_iam_policy
}

################################################################################

output "lambda_iam_ids" {
  value = { for k, v in aws_iam_role.scope : k => v.unique_id }
}

output "lambda_iam_arns" {
  value = { for k, v in aws_iam_role.scope : k => v.arn }
}

output "lambda_iam_names" {
  value = { for k, v in aws_iam_role.scope : k => v.name }
}

output "lambda_iam_create_dates" {
  value = { for k, v in aws_iam_role.scope : k => v.create_date }
}

output "lambda_iam_descriptions" {
  value = { for k, v in aws_iam_role.scope : k => v.description }
}

################################################################################

output "lambda_hashs" {
  value = { for k, v in aws_lambda_function.scope : k => v.source_code_hash }
}

output "lambda_base_arns" {
  value = { for k, v in aws_lambda_function.scope : k => v.arn }
}

output "lambda_full_arns" {
  value = { for k, v in aws_lambda_function.scope : k => v.qualified_arn }
}

output "lambda_invoke_arns" {
  value = { for k, v in aws_lambda_function.scope : k => v.invoke_arn }
}

output "lambda_modified_dates" {
  value = { for k, v in aws_lambda_function.scope : k => v.last_modified }
}

################################################################################
