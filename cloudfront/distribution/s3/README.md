<!----------------------------------------------------------------------------->

# cloudfront/distribution/s3

#### [CloudFront] distributions that have a [S3] bucket origin

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/cdn/aws//cloudfront/distribution/s3`**

--------------------------------------------------------------------------------

**NOTE**: This module requires that the **aws** provider is configured for
region `us-east-1`. For details on how to call individual modules with
specific providers, please [see here](https://www.terraform.io/upgrade-guides/0-11.html#interactions-between-providers-and-modules).

--------------------------------------------------------------------------------

### Example Usage

```
variable "owner"   { default = "terraform@bitservices.io" }
variable "company" { default = "BITServices Ltd"          }

provider "aws" {
  alias  = "cloudfront"
  region = "us-east-1"
}

module "my_cloudfront_identity" {
  source = "gitlab.com/bitservices/cdn/aws//cloudfront/identity"

  providers = {
    "aws" = "aws.cloudfront"
  }
}

module "my_s3_bucket" {
  source  = "gitlab.com/bitservices/storage/aws//s3/bucket"
  class   = "foobar-io"
  owner   = var.owner
  company = var.company

  providers = {
    "aws" = "aws.cloudfront"
  }
}

module "my_s3_policy" {
  source = "gitlab.com/bitservices/storage/aws//s3/policy"
  bucket = module.my_s3_bucket.name
  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Example permissions",
            "Effect": "Allow",
            "Principal": {
                "AWS": "${module.my_cloudfront_identity.iam_arn}"
            },
            "Action": [
                "s3:GetObject",
                "s3:ListBucket"
            ],
            "Resource": [
                "arn:aws:s3:::${module.my_s3_bucket.name}",
                "arn:aws:s3:::${module.my_s3_bucket.name}/*"
            ]
        }
    ]
}
POLICY

  providers = {
    "aws" = "aws.cloudfront"
  }
}

module "my_cloudfront_distribution_s3" {
  source         =   "gitlab.com/bitservices/cdn/aws//cloudfront/distribution/s3"
  name           =   "foobar-io"
  domains        = [ "foobar.io", "www.foobar.io" ]
  owner          =   var.owner
  company        =   var.company
  s3_identity    =   module.my_cloudfront_identity.access_identity_path
  s3_domain_name =   module.my_s3_bucket.domain_name

  providers = {
    "aws" = "aws.cloudfront"
  }
}
```

--------------------------------------------------------------------------------

### lambda_functions Object Example
```
[{
  "bucket"       = "lambda-edge-us-east-1-bits"
  "event"        = "origin-request"
  "handler"      = "index.handler"
  "publish"      = true
  "runtime"      = "nodejs8.10"
  "version"      = "0.0.1"
  "function"     = "my-origin-request-function"
  "memory_mb"    = 128
  "description"  = "Managed by Terraform"
  "timeout_secs" = 5
},
{
  "bucket"       = "lambda-edge-us-east-1-bits"
  "event"        = "viewer-request"
  "handler"      = "index.handler"
  "publish"      = true
  "runtime"      = "nodejs8.10"
  "version"      = "0.0.5"
  "function"     = "my-viewer-request-function"
  "memory_mb"    = 128
  "description"  = "Managed by Terraform"
  "timeout_secs" = 5
}]
```

<!----------------------------------------------------------------------------->

[S3]:         https://aws.amazon.com/s3/
[Route53]:    https://aws.amazon.com/route53/
[CloudFront]: https://aws.amazon.com/cloudfront/

<!----------------------------------------------------------------------------->
