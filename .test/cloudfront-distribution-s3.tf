###############################################################################
# Modules
###############################################################################

module "cloudfront_distribution_s3" {
  source         = "../cloudfront/distribution/s3"
  name           = "foobar-io"
  domains        = ["foobar.io", "www.foobar.io"]
  owner          = local.owner
  company        = local.company
  s3_identity    = module.cloudfront_identity.access_identity_path
  s3_domain_name = "foobar-io-us-east-1-bits.s3.amazonaws.com"
}

###############################################################################
