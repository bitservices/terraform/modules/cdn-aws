<!----------------------------------------------------------------------------->

# cdn (aws)

<!----------------------------------------------------------------------------->

## Description

Manage content delivery network (CDN) components with [AWS] [CloudFront].

<!----------------------------------------------------------------------------->

## Modules

* [cloudfront/distribution/s3](cloudfront/distribution/s3/README.md) - [CloudFront] distributions that have a [S3] bucket origin.
* [cloudfront/identity](cloudfront/identity/README.md) - Origin access identities for allowing [CloudFront] access to resources.

<!----------------------------------------------------------------------------->

[S3]:         https://aws.amazon.com/s3/
[AWS]:        https://aws.amazon.com/
[CloudFront]: https://aws.amazon.com/cloudfront/


<!----------------------------------------------------------------------------->
